package com.spring.io.java.basics.rinvay.mp;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

public class MyBatisPlusGeneratorTool {
    public static void main(String[] args) {
        /*
         * 创建 Generator 对象
         */
        AutoGenerator autoGenerator = new AutoGenerator();

        /*
         * 数据源
         */
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL);
        dataSourceConfig.setUrl("jdbc:mysql://127.0.0.1:3306/my_batis_plus_MP?useSSL=false&useUnicode=true&characterEncoding=UTF-8&serverTimeZone=GMT%2B8");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("Rinvay520");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        /*
         * 装载/填充 - 数据源
         */
        autoGenerator.setDataSource(dataSourceConfig);
        /*
         * 全局配置
         */
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(System.getProperty("user.dir")+"/rinvay-mybatis-plus-mp/src/main/java");
        /*
         * setOpen 状态 标识 创建成功后文件是否打开
         * 默认是打开的 这里设置 false 不打开
         */
        globalConfig.setOpen(false);

        /*标识是否覆盖原先-生成的字段  false 不覆盖*/
        globalConfig.setFileOverride(false);
        globalConfig.setIdType(IdType.ASSIGN_ID);
        globalConfig.setDateType(DateType.ONLY_DATE);
        globalConfig.setSwagger2(true);
        /*通常标识-查询映射结果 默认：false [体现于mapper.xml内]*/
        globalConfig.setBaseResultMap(true);
        /*通常标识-查询结果列 默认：false [体现于mapper.xml内]*/
        globalConfig.setBaseColumnList(true);
        /*开启语法糖*/
        globalConfig.setActiveRecord(true);
        /*目的是去掉 I */
        //globalConfig.setControllerName("%Controller");
        //globalConfig.setServiceName("%Service");
        //globalConfig.setServiceImplName("%Service");
        globalConfig.setAuthor("RINVAY");

        /*
         * 装载/填充 -全局配置
         */
        autoGenerator.setGlobalConfig(globalConfig);
        /*
         * 包信息
         */
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent("com.rinvay.springcloud");
        packageConfig.setModuleName("generator");
        packageConfig.setController("controller");
        packageConfig.setService("service");
        packageConfig.setServiceImpl("service.impl");
        packageConfig.setMapper("mapper");
        //packageConfig.setXml(System.getProperty("user.dir")+"/rinvay-mybatis-plus-mp/src/main/resources/com/rinvay/springcloud/generator/mapper/");
        packageConfig.setEntity("entity");
        /*
         * 装载/填充 -包信息
         */
        autoGenerator.setPackageInfo(packageConfig);
        /*
         * 配置策略
         */
        StrategyConfig strategyConfig = new StrategyConfig();
        /*
         * 针对自动生成的数据库表名指定
         */
        strategyConfig.setInclude("t_repository_purchase_detail",
                "t_repository_purchase",
                "t_repository_inventory",
                "t_repository_exit_detail",
                "t_repository_exit",
                "t_repository_change",
                "t_repository_access_detail",
                "t_repository_access",
                "t_repository_inventory_detail");

        /*
         * 标识-添加后 自动添加 Lombok注解
         */


        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setTablePrefix("t_");
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setChainModel(true);
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.setControllerMappingHyphenStyle(true);
        strategyConfig.setEntitySerialVersionUID(false);



        strategyConfig.setLogicDeleteFieldName("deleted");
        //逻辑删除名字
        TableFill gmtCreate = new TableFill("gmt_create", FieldFill.INSERT);
        TableFill gmtModified = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
        //自动填充策略配置
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(gmtCreate);
        tableFills.add(gmtModified);
        strategyConfig.setTableFillList(tableFills);
        // 乐观锁配置策略
        strategyConfig.setVersionFieldName("version");

        // 开启驼峰命名格式
        //strategyConfig.setRestControllerStyle(true);
        //strategyConfig.setControllerMappingHyphenStyle(true);  链接请求更变： localhost:8080/hello_id_2

        /*
         * 装载/填充 -配置策略
         */
        autoGenerator.setStrategy(strategyConfig);

        /*
         * 执行  Generator 对象
         */
        autoGenerator.execute();

    }
}
