package com.spring.io.java.basics.rinvay.mp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RinMyBatisPlusMPApplication {
    public static void main(String[] args) {
        SpringApplication.run(RinMyBatisPlusMPApplication.class,args);
    }
}
